# vim: ft=make

#? Environment ------------------------------------------------------------------------------------------------------->
SHELL 	:= /usr/bin/env bash
OS 		:= $(shell { uname -s | tr '[:upper:]' '[:lower:]' 2>/dev/null || echo "unknown"; })
ARCH 	:= $(shell { uname -m | tr '[:upper:]' '[:lower:]' 2>/dev/null || echo "unknown"; })
LIB_EXTENSION := $(shell { [ "$(OS)" == "darwin" ] && echo "dylib" || echo "so"; })

ifeq ($(ARCH), x86_64)
	ARCH := amd64
endif
#? End Environment --------------------------------------------------------------------------------------------------->

#? Local ------------------------------------------------------------------------------------------------------------->

#? End Local --------------------------------------------------------------------------------------------------------->
.PHONY: bootstrap
bootstrap:
	@mkdir -p ./.local
	@git clone https://gitlab.com/kureq/aves/aves-go.git ./.local/aves-go

.PHONY: build
build:
	@cd ./.local/aves-go/ && make build

.PHONY: old
old:
	@go build -C ./src/aves -o ./../../lib/ffi/aves-$(OS)-$(ARCH).$(LIB_EXTENSION) -buildmode=c-shared .
	@printf "Created release file, Size %s, Path %s\n" $$(du -sh ./lib/ffi/aves-$(OS)-$(ARCH).$(LIB_EXTENSION))

.PHONY: download
download: $(LOCAL_DIR)
	@git clone https://git.savannah.gnu.org/git/bash.git $(LOCAL_DIR)/bash
