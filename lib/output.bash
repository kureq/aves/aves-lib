# vim: ft=sh

#? Loaded ------------------------------------------------------------------------------------------------------------>
if [[ -n "${AVES_CORE-}" ]]; then
  : debug "Library %h'%${BASH_SOURCE[0]##*.}%n' already loaded"
  return 0
fi
#? End Loaded -------------------------------------------------------------------------------------------------------->

#? Colors ------------------------------------------------------------------------------------------------------------>
## Reset variables
declare -xgr AVES_NOFORMAT=$'\033[0m'
declare -xgr AVES_NOFORMAT_BOLD=$'\033[1m'
declare -xgr AVES_NOFORMAT_DIM=$'\033[2m'
declare -xgr AVES_NOFORMAT_UNDERLINED=$'\033[4m'
declare -xgr AVES_NOFORMAT_BLINK=$'\033[5m'
declare -xgr AVES_NOFORMAT_INVERTED=$'\033[7m'
declare -xgr AVES_NOFORMAT_HIDDEN=$'\033[8m'

## Format
declare -xgr AVES_FORMAT_DEFAULT=0
declare -xgr AVES_FORMAT_BOLD=1
declare -xgr AVES_FORMAT_DIM=2
declare -xgr AVES_FORMAT_UNDERLINED=4
declare -xgr AVES_FORMAT_BLINK=5
declare -xgr AVES_FORMAT_INVERTED=7
declare -xgr AVES_FORMAT_HIDDEN=8

declare -xgr AVES_COLOR_MODE=256
declare -xgr AVES_COLOR_PREFIX="\e["
declare -xgr AVES_COLOR_FG_MID=";38;5;"
declare -xgr AVES_COLOR_BG_MID=";48;5;"
declare -xgr AVES_COLOR_SUFFIX="m"

declare -xgr AVES_OUTPUT_INDENT="             "
#? End Environment Variables ----------------------------------------------------------------------------------------->

#? Functions --------------------------------------------------------------------------------------------------------->
#? Parse Colors ------------------------------------------------------------------------------------------------------>

#######################################
# Set the the colors for aves. It sets colors in the aves namespace.
# The colors are set based on the theme and color mode.
# Themes are also set as environment variables using the AVES_OUTPUT_ prefix.
#
# Usage: _aves_set_colors
#
#######################################
_aves_set_colors() {

  local -A _aves_set_colors_arr

  local _aves_color_key _aves_c_val _aves_style_key

  while puffin iterate -p aves -n i -s -v _aves_color_key -v _aves_color_val "$.aves.theme.colors"; do
    _aves_c_val=

    puffin get -i _aves_color_val -v _aves_c_val "${AVES_COLOR_MODE}"

    if [[ -z "${_aves_c_val:-}" ]]; then
      # Color was not found for MODE
      continue
    fi

    puffin set -p aves -k "aves.colors.${_aves_color_key}" "${AVES_COLOR_PREFIX}0${AVES_COLOR_FG_MID}${_aves_c_val:-}${AVES_COLOR_SUFFIX}"
    _aves_set_colors_arr["${_aves_color_key}"]="${_aves_c_val:-}"
    declare -gx "AVES_COLORS_${_aves_color_key^^}=${AVES_COLOR_PREFIX}0${AVES_COLOR_FG_MID}${_aves_c_val:-}${AVES_COLOR_SUFFIX}"
    # printf -- "%b%s${AVES_NOFORMAT}\n" "${AVES_COLOR_PREFIX}0${AVES_COLOR_FG_MID}${_aves_c_val:-}${AVES_COLOR_SUFFIX}" "${_aves_color_key}"
  done

  while puffin iterate -p aves -n avc --split -v _aves_style_key -v _aves_style_opts "$.aves.theme.style"; do
    # GET/SET Variables using puffin

    # Normal Style
    puffin get -i _aves_style_opts -v _fg -v _bg -v _style '$["fg", "bg", "style"]'
    _n_style="${_style:+AVES_FORMAT_${_style^^}}"
    _style="${_style:+${!_n_style:-0}}"
    _c_normal_fg="${_fg:+${AVES_COLOR_PREFIX}${_style:-0}${AVES_COLOR_FG_MID}${_aves_set_colors_arr["${_fg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_normal_bg="${_bg:+${AVES_COLOR_PREFIX}${_style:-0}${AVES_COLOR_BG_MID}${_aves_set_colors_arr["${_bg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_normal="${_c_normal_fg:-}${_c_normal_bg:-}"
    puffin set -p aves -k "aves.style.${_aves_style_key}.style" "${_c_normal}"

    # Highlight Style
    puffin get -i _aves_style_opts -v _h_fg -v _h_bg -v _h_style '$.highlight["fg", "bg", "style"]'
    _h_n_style="${_h_style:+AVES_FORMAT_${_h_style^^}}"
    _h_style="${_h_style:+${!_h_n_style:-0}}"
    _c_highlight_fg="${_h_fg:+${AVES_COLOR_PREFIX}${_h_style:-0}${AVES_COLOR_FG_MID}${_aves_set_colors_arr["${_h_fg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_highlight_bg="${_h_bg:+${AVES_COLOR_PREFIX}${_h_style:-0}${AVES_COLOR_BG_MID}${_aves_set_colors_arr["${_h_bg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_highlight="${_c_highlight_fg:-}${_c_highlight_bg:-}"
    puffin set -p aves -k "aves.style.${_aves_style_key}.highlight" "${_c_highlight}"

    # Dim Style
    # NOTE(VBE): Is DIM the right word here?
    puffin get -i _aves_style_opts -v _d_fg -v _d_bg -v _d_style '$.dim["fg", "bg", "style"]'
    _dimn_style="${_d_style:+AVES_FORMAT_${_d_style^^}}"
    _d_style="${_dstyle:+${!_dimn_style:-0}}"
    _c_dim_fg="${_d_fg:+${AVES_COLOR_PREFIX}${_d_style:-0}${AVES_COLOR_FG_MID}${_aves_set_colors_arr["${_d_fg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_dim_bg="${_d_bg:+${AVES_COLOR_PREFIX}${_d_style:-0}${AVES_COLOR_BG_MID}${_aves_set_colors_arr["${_d_bg:-}"]:-}${AVES_COLOR_SUFFIX}}"
    _c_dim="${_c_dim_fg:-}${_c_dim_bg:-}"
    puffin set -p aves -k "aves.style.${_aves_style_key}.dim" "${_c_dim}"
  done
}
#? End Parse Colors -------------------------------------------------------------------------------------------------->

#? Replace Me! ------------------------------------------------------------------------------------------------------->
#######################################
# A simple function to replace runes in a string, with environment variables.
# This function replaces the following runes:
#   - %h - _highlight
#   - %r - _style or AVES_NOFORMAT
#   - %d - _dim
#
# Usage: _aves::parse_string string
#
# Arguments:
#  nameref string - The string to replace runes in.
#
#######################################
_aves_parse_string() {
  # TODO(VBE): Move to an FFI Aves function
  local -n _ref="${1}"
  local _line

  _line="${_ref:-}"
  _line="${_line//%h/${_highlight-}}"
  _line="${_line//%r/${_style:-${AVES_NOFORMAT-}}}"
  _line="${_line//%d/${_dim-}}"

  _ref="${_line}"
}
#? End Replace Me! --------------------------------------------------------------------------------------------------->

#? Output Func ------------------------------------------------------------------------------------------------------->

#######################################
# A very controversial output function, it extends the functionality of the built-in ':' function.
# It allows for structured output, with different levels of verbosity and formatting.
#
# Usage: : level [options] [message]
#
# Levels:
#   debug     - Debugging messages
#   verbose   - Verbose messages
#   info      - Informational messages
#   warning   - Warning messages
#   error     - Error messages
#   header    - Header messages
#   important - Important messages
#
# Examples:
#  : debug "This is a debug message"
#  : verbose "This is a verbose message"
#  : info "This is an informational message"
#  : warning "This is a warning message"
#  : error "This is an error message"
#
# Returns:
#  0 - Always to mimic the built-in ':' function
#
# Output:
#  The output is sent to STDERR and is styled based on the format of the message.
#
#######################################
:() {
  # Supporting the default : behavior
  if [[ -z ${1:-} ]]; then
    return 0
  fi

  if [[ ${DEBUG:-} -le 0 || "${DEBUG:-}" == false ]]; then
    return 0
  fi

  case "${1}" in
    debug | verbose | info | warning | error | fatal | header | success | fail | highlight | stacktrace) ;;
    *)
      return 0
      ;;
  esac

  local _level _title_style _highlight _dim_style _style _format _str _title

  puffin get -p "aves" \
    -v _highlight \
    -v _dim_style \
    -v _style \
    '$.aves.style.'"${1}"'["highlight", "dim", "style", "level", "format"]'
  puffin get -p "aves" \
    -v _level \
    -v _format \
    '$.aves.output.'"${1}"'["level", "format"]'

  if [[ ${_level:-0} -le ${DEBUG:-0} ]]; then
    case ${_format-} in
      1)
        _title="${1^^}"
        _str="${2}"
        _aves_parse_string _str

        printf >&2 -- "${AVES_NOFORMAT-}%b%12s ${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
          "${_highlight-}" "${_title}" "${_style-}" "${_str}"

        for _str in "${@:3}"; do
          [[ -z "${_str}" ]] && continue
          _aves_parse_string _str
          printf >&2 -- "${AVES_NOFORMAT-}${AVES_OUTPUT_INDENT-}%b%b${AVES_NOFORMAT-}\n" \
            "${_style-}" "${_str}"
        done
        ;;
      2 | 3)
        printf -v _wrap "%.s─" {1..80}

        if [[ "${_format-}" == 3 ]]; then
          printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
            "${_highlight-}" "${_wrap}"
        fi

        _str="${2}"
        _aves_parse_string _str

        printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
          "${_style-}" "${_str}"

        for _str in "${@:3}"; do
          [[ -z "${_str}" ]] && continue
          _aves_parse_string _str

          printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
            "${_style-}" "${_str}"
        done

        printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
          "${_highlight-}" "${_wrap}"

        ;;
      *)
        _str="${2}"
        _aves_parse_string _str
        printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
          "${_style-}" "${_str}"

        for _str in "${@:3}"; do
          [[ -z "${_str}" ]] && continue
          _aves_parse_string _str
          printf >&2 -- "${AVES_NOFORMAT-}%b%b${AVES_NOFORMAT-}\n" \
            "${_style-}" "${_str}"
        done
        ;;
    esac
  fi
}

# TODO(VBE): Missing documentation
stacktrace() {
  local error_code=${?}
  [[ ${error_code} -eq 0 && -n ${_aves_ec:-} ]] && error_code=${_aves_ec}
  local _aves_err_name

  [[ ${error_code} -eq 0 ]] && return 0

  local i=0
  local stack_size=${#FUNCNAME[@]}
  local err_arr=()

  local func line src error_name

  for ((i = 1; i < stack_size; i++)); do
    func="${FUNCNAME[$i]}"
    line="${BASH_LINENO[$i - 1]}"
    src="${BASH_SOURCE[$i]}"

    err_arr+=("%h$func%r (%h$src%r:%h$line%r)")
  done

  aves_exit_code_name error_name "${error_code}"
  : stacktrace "Error %h${error_code}%r ${error_name+(%h${error_name-}%r) }occurred:" \
    "${err_arr[@]}"
}

# TODO(VBE): Missing documentation
aves_err_trap() {
  local error_code=${?}
  [[ ${error_code} -eq 0 && -n ${_aves_ec:-} ]] && error_code=${_aves_ec}
  local error_name

  aves_exit_code_name error_name "${error_code}"
  : error "Error: %h${error_code}%r ${error_name+(%h${error_name-}%r) }occurred in:" \
    "file: %h${BASH_SOURCE[1]}%r, line: %h${BASH_LINENO[0]}%r" \
    "function: %h${FUNCNAME[1]}%r, command: %h${BASH_COMMAND}" \
    "${@-}"
}

aves_print_bird() {
  local size
  if [[ -z "${1:-}" ]]; then
    size=52
  else
    size="${1}"
  fi

  local puffin_bird
  puffin_bird=$(<"${AVES_LIB_DIR}/assets/puffin_${size}.ansi")
  printf >&2 -- "%b\n" "${puffin_bird}"
}
#? End Output Func --------------------------------------------------------------------------------------------------->
#? End Functions ----------------------------------------------------------------------------------------------------->

#? Export ------------------------------------------------------------------------------------------------------------>
export -f : stacktrace aves_err_trap aves_print_bird
readonly -f : stacktrace aves_err_trap aves_print_bird
#? End Export -------------------------------------------------------------------------------------------------------->
