# vim: ft=sh

#? Loaded ------------------------------------------------------------------------------------------------------------>
if [[ -n "${AVES_CORE-}" ]]; then
  : debug "Library %h'%${BASH_SOURCE[0]##*.}%n' already loaded"
  return 0
fi
#? End Loaded -------------------------------------------------------------------------------------------------------->

# Yaml Example:
# - name: bin-linux
#   path: "${DOTS_DIR}/${name}"
#   git: "${DOTS_REMOTE}/${name}.git"
#   branch: main
#   dependencies: "${project}/dependencies.yaml"
#   symlink:
#     - mode: "files"
#       from: "${project}/bin/${file}"
#       to: "${HOME}/bin/${file}"
#   contraints:
#     - "os == 'linux'"

#? Environment Variables --------------------------------------------------------------------------------------------->

#? End Environment Variables ----------------------------------------------------------------------------------------->

#? Functions --------------------------------------------------------------------------------------------------------->

#? Strategies -------------------------------------------------------------------------------------------------------->
_aves_internal_strategy_handler() {
  local _pcms_sh_node
  _pcms_sh_node="${1:-}"

  if [[ -z "${_pcms_sh_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  if puffin get -i _pcms_sh_node -f "json" '$.curl' --error >/dev/null; then
    : debug "Using Curl Strategy"
    _aves_internal_curl_handler _pcms_sh_node && {
      : info "Successfully resolved %h'${_pcms_sh_node:-}'"
      return "${AVES_EXIT_SUCCESS}"
    }
  fi

  if puffin get -i _pcms_sh_node -f "json" '$.message' --error >/dev/null; then
    : debug "Using Message Strategy"
    _aves_internal_message_handler _pcms_sh_node
    local _exit_code=${?}
    if [[ ${_exit_code} -gt 0 ]]; then
      return "${_exit_code}"
    fi
  fi

  if puffin get -i _pcms_sh_node -f "json" '$.script' --error >/dev/null; then
    : debug "Using Script Strategy"
    _aves_internal_script_handler _pcms_sh_node

    local _exit_code=${?}
    if [[ ${_exit_code} -gt 0 ]]; then
      return "${_exit_code}"
    fi
  fi
}

_aves_internal_script_handler() {
  local -n _pcms_script_node
  _pcms_script_node="${1:-}"

  if [[ -z "${_pcms_script_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  # become: true
  # command: pacman -S bash

  local _pcms_sh_become _pcms_sh_command

  puffin get -i _pcms_script_node -f "json" -v _pcms_sh_become -v _pcms_sh_command \
    '$.script["become", "command"]'

  if [[ -z "${_pcms_sh_command:-}" ]]; then
    : error "Command is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  if [[ "${_pcms_sh_become:-}" == "true" ]]; then
    : debug "Executing %h'${_pcms_sh_command:-}'%r with sudo"
    # shellcheck disable=SC2086
    sudo ${_pcms_sh_command} || {
      : error "Failed to execute %h'${_pcms_sh_command:-}'%r"
      return "${AVES_EXIT_GENERAL_ERROR}"
    }
  else
    : debug "Executing %h'${_pcms_sh_command:-}'%r"
    ${_pcms_sh_command} || {
      : error "Failed to execute %h'${_pcms_sh_command:-}'%r"
      return "${AVES_EXIT_GENERAL_ERROR}"
    }
  fi
}

_aves_internal_message_handler() {
  local -n _pcmm_mh_node
  _pcmm_mh_node="${1:-}"

  if [[ -z "${_pcmm_mh_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  # message.msg -> _pcmm_mh_message
  # message.level -> _pcmm_mh_level

  local _pcmm_mh_message _pcmm_mh_level _pcmm_mh_action

  puffin get -i _pcmm_mh_node -f "json" -v _pcmm_mh_message -v _pcmm_mh_level -v _pcmm_mh_action \
    '$.message["msg", "level", "action"]'

  : "${_pcmm_mh_level:-info}" "${_pcmm_mh_message:-}"
  if [[ -n "${_pcmm_mh_action:-}" ]]; then
    case "${_pcmm_mh_action}" in
      *[eE][xX][iI][tT]* | *[pP][aA][nN][iI][cC]*)
        return "${AVES_EXIT_GENERAL_ERROR}"
        ;;
    esac
  fi
}

_aves_internal_extract_handler() {
  local _pcme_eh_ref
  _pcme_eh_ref="${1:-}"

  local -n _pcme_eh_node
  _pcme_eh_node="${2:-}"

  local -n _input
  _input="${3:-}"

  if [[ -z "${_pcme_eh_ref:-}" ]]; then
    : error "Ref is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  if [[ -z "${_pcme_eh_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  if [[ -z "${_input:-}" ]]; then
    : error "Target is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  local _pcme_eh_path _pcme_eh_using _pcme_eh_cleanup

  puffin get -i _pcme_eh_node -f "json" -v _pcme_eh_path -v _pcme_eh_using -v _pcme_eh_cleanup \
    '$.'"${_pcme_eh_ref}"'.extract["path", "using", "cleanup"]'

  if [[ -z "${_pcme_eh_path:-}" ]]; then
    : warning "Path not set for %h'${_pcme_eh_node:-}'%r, defaulting to %h'${AVES_DIR_BIN:-}'"
    _pcme_eh_path="${AVES_DIR_BIN:-}/"
  fi

  case "${_pcme_eh_using:-}" in
    tar)
      : verbose "Extracting %h'${_input:-}'%r to %h'${_pcme_eh_path:-}'%r"
      tar -xzf "${_input}" -C "${_pcme_eh_path}" || {
        : error "Failed to extract %h'${_input:-}'%r, to %h'${_pcme_eh_path:-}'%r"
        return "${AVES_EXIT_GENERAL_ERROR}"
      }
      ;;

    unzip | zip)
      : verbose "Extracting %h'${_input:-}'%r to %h'${_pcme_eh_path:-}'%r"
      unzip -q "${_input}" -d "${_pcme_eh_path}" || {
        : error "Failed to extract %h'${_input:-}'%r, to %h'${_pcme_eh_path:-}'%r"
        return "${AVES_EXIT_GENERAL_ERROR}"
      }
      ;;

    7z)
      : verbose "Extracting %h'${_input:-}'%r to %h'${_pcme_eh_path:-}'%r"
      7z x -o"${_pcme_eh_path}" "${_input}" || {
        : error "Failed to extract %h'${_input:-}'%r, to %h'${_pcme_eh_path:-}'%r"
        return "${AVES_EXIT_GENERAL_ERROR}"
      }
      ;;

      # TODO(VBE): Add more extraction methods, and maybe 'command'
    *)
      : error "Unsupported extraction method %h'${_pcme_eh_using:-}'%r"
      return "${AVES_EXIT_GENERAL_ERROR}"
      ;;
  esac

  if [[ "${_pcme_eh_cleanup:-}" == "true" ]]; then
    : debug "Cleaning up %h'${_input:-}'%r"
    \rm -f "${_input}" || {
      : warning "Failed to remove %h'${_input}'%r"
      return "${AVES_EXIT_GENERAL_ERROR}"
    }
  fi

  _input="${_pcme_eh_path}"
  return "${AVES_EXIT_SUCCESS}"
}

_aves_internal_mod_hander() {
  local _pcmmh_ref
  _pcmmh_ref="${1:-}"

  local -n _pcmmh_node
  _pcmmh_node="${2:-}"

  if [[ -z "${_pcmmh_ref:-}" ]]; then
    : error "Ref is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  if [[ -z "${_pcmmh_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  local _pcmmh_file _pcmmh_rename _pcmmh_chmod

  while puffin iterate -i _pcmmh_node -f "json" -n _pcmmhi \
    -v _pcmmh_file -v _pcmmh_rename -v _pcmmh_chmod \
    '$.'"${_pcmmh_ref}"'.mod[*]["file", "rename", "chmod"]'; do

    : debug "Modifying %h'${_pcmmh_file:-}'%r" "renaming to %h'${_pcmmh_rename:-}'%r" "chmod %h'${_pcmmh_chmod:-}'%r"

    if [[ -n "${_pcmmh_rename:-}" ]]; then
      : debug "Renaming %h'${_pcmmh_file:-}'%r to %h'${_pcmmh_rename:-}'%r"
      \mv -f "${_pcmmh_file}" "${_pcmmh_rename}" || {
        : error "Failed to rename %h'${_pcmmh_file:-}'%r to %h'${_pcmmh_rename:-}'%r"
        return "${AVES_EXIT_GENERAL_ERROR}"
      }

      _pcmmh_file="${_pcmmh_rename}"
    fi

    if [[ -n "${_pcmmh_chmod:-}" ]]; then
      : debug "Changing mode of %h'${_pcmmh_file:-}'%r to %h'${_pcmmh_chmod:-}'%r"
      \chmod "${_pcmmh_chmod}" "${_pcmmh_file}" || {
        : error "Failed to change mode of %h'${_pcmmh_file:-}'%r to %h'${_pcmmh_chmod:-}'%r"
        return "${AVES_EXIT_GENERAL_ERROR}"
      }
    fi

  done

}

_aves_internal_curl_handler() {
  local -n _pcmc_ch_node
  _pcmc_ch_node="${1:-}"

  if [[ -z "${_pcmc_ch_node:-}" ]]; then
    : error "Node is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  local _pcmc_ch_url _pcmc_ch_path _pcmc_ch_extract

  puffin get -i _pcmc_ch_node -f "json" -v _pcmc_ch_url -v _pcmc_ch_path -v _pcmc_ch_extract \
    '$.curl["url", "path"]'

  : info "Downloading %h'${_pcmc_ch_url}'%r, to %h'${_pcmc_ch_path:-${AVES_TEMP_DIR}}'%r"

  # TODO(VBE): Handle Not Found

  curl --fail --silent --show-error -L "${_pcmc_ch_url}" >"${_pcmc_ch_path:-${AVES_TEMP_DIR}}" 2>&1 || {
    local _curl_err="${?}"
    local err_msg
    err_msg=$(<"${_pcmc_ch_path:-${AVES_TEMP_DIR}}")

    : error "Failed to download %h'${_pcmc_ch_url}'%r, exit code: %h'${_curl_err}'%r" "%h${err_msg:-}"
    return "${AVES_EXIT_GENERAL_ERROR}"
  }

  if puffin get -i _pcmc_ch_node -f "json" '$.curl.extract' --error >/dev/null; then
    : debug "Extracting %h'${_pcmc_ch_path:-}'%r"
    if ! _aves_internal_extract_handler "curl" _pcmc_ch_node _pcmc_ch_path; then
      return "${AVES_EXIT_GENERAL_ERROR}"
    fi
  fi

  if puffin get -i _pcmc_ch_node -f "json" '$.curl.mod' --error >/dev/null; then
    : debug "Modifying %h'${_pcmc_ch_path:-}'%r"
    if ! _aves_internal_mod_hander "curl" _pcmc_ch_node _pcmc_ch_path; then
      return "${AVES_EXIT_GENERAL_ERROR}"
    fi
  fi

  return "${AVES_EXIT_SUCCESS}"
}
#? End Strategies ---------------------------------------------------------------------------------------------------->

dependency() {
  local _pcm_id _pcm_version _pcm_arch_map _pcm_resolve

  # 1 -> id, 2 -> version, 3 -> arch_map, 4 -> resolve
  # PERF(VBE): Atm we are only supporting the strategy implementation (Yaml)
  if [[ "${#}" -ne 4 ]]; then
    : error "Missing required arguments for %h'${FUNCNAME[0]}' found %h'${#}'%r expected %h'4'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  _pcm_id="${1}"
  _pcm_version="${2:-}"
  _pcm_arch_map="${3:-}"
  _pcm_resolve="${4:-}"

  : debug "Checking %h'${_pcm_id}'%r"

  local id
  id="${_pcm_id}"

  if [[ -z "${os:-}" ]]; then
    : debug "Operating System is not set, using %h'${AVES_PLATFORM:-}'"
    os="${AVES_PLATFORM:-}"
  fi

  if [[ -z "${arch:-}" ]]; then
    : debug "Architecture is not set, using %h'${AVES_ARCHITECTURE:-}'"
    arch="${AVES_ARCHITECTURE:-}"
  fi

  local version current_version semver

  if ! _aves_internal_get_version _pcm_version; then
    # PERF(VBE): Could do a custom exit code check handling here
    : debug "%h'${_pcm_id}'%r version check failed${_pcm_installed_version:+, installed version ${_pcm_installed_version}}"
  else
    : debug "Version check passed, %h'${_pcm_id}'%r"
    return "${AVES_EXIT_SUCCESS}"
  fi

  local _pcm_puffin_id
  _pcm_puffin_id="_pcm_puff_$$_${RANDOM}"

  puffin read -p "${_pcm_puffin_id}" -t "json" _pcm_resolve || {
    : error "Failed to read %h'${_pcm_resolve}'%r"
    puffin forget "${_pcm_puffin_id}"
    return "${AVES_EXIT_GENERAL_ERROR}"
  }

  : debug "Resolving %h'${_pcm_id}'%r with %h'${_pcm_resolve}'%r, %h'${os}.${arch}'%r"

  local _pcm_strat

  # OS Arch Strategy
  if puffin get -p "${_pcm_puffin_id}" -v _pcm_strat "$.${os}_${arch}" --error >/dev/null; then
    : debug "Resolving %h'${_pcm_id}'%r with %h'${os}_${arch}'%r strategy"
    _aves_internal_strategy_handler "${_pcm_strat}"

  # OS distribution Strategy
  elif puffin get -p "${_pcm_puffin_id}" -v _pcm_strat "$.${os}_${distribution}" --error >/dev/null; then
    : debug "Resolving %h'${_pcm_id}'%r with %h'${os}_${distribution}'%r strategy"
    _aves_internal_strategy_handler "${_pcm_strat}"

  # OS Strategy
  elif puffin get -p "${_pcm_puffin_id}" -v _pcm_strat "$.${os}" --error >/dev/null; then
    : debug "Resolving %h'${_pcm_id}'%r with %h'${os}'%r strategy"
    _aves_internal_strategy_handler "${_pcm_strat}"

  # Default to 'default' strategy, if available
  elif puffin get -p "${_pcm_puffin_id}" -v _pcm_strat "$.default" --error >/dev/null; then
    : debug "Resolving %h'${_pcm_id}'%r with %h'default'%r strategy"
    _aves_internal_strategy_handler "${_pcm_strat}"

    # local _exit_code="${?}"
    # if [[ ${_exit_code} -gt 0 ]]; then
    #   puffin forget "${_pcm_puffin_id}"
    #   return "${_exit_code}"
    # fi

  else
    : warning "Failed to get resolve strategy for %h'${_pcm_id:-}'"
    puffin forget "${_pcm_puffin_id}"
    return "${AVES_EXIT_GENERAL_ERROR}"
  fi

  # Unload the config
  puffin forget "${_pcm_puffin_id}"
  return "${AVES_EXIT_SUCCESS}"
}

resolve_dependencies() {
  local puffin_ref
  # Atm we are only supporting the strategy implementation (Yaml)
  if [[ -z "${1-}" ]]; then
    : error "Puffin reference variable is required for %h'${FUNCNAME[0]}'"
    return "${AVES_EXIT_MISSING_REQUIRED_ARGUMENTS}"
  fi

  : info "Resolving dependencies" "${pcm_deps:-}"

  local os arch version distribution
  os="${AVES_PLATFORM:-}"
  arch="${AVES_ARCHITECTURE:-}"
  distribution="${AVES_DISTRIBUTION:-}"

  : debug "Operating System %h'${os}'%r" "Architecture %h'${arch}'%r" "Distribution %h'${distribution}'%r"

  while puffin iterate -p "${1}" -f "json" -n pcm_deps --array \
    -v _pcm_id -v _pcm_version -v _pcm_arch_map -v _pcm_resolve \
    "dependencies[*]['id','version','arch_map','resolve']"; do

    : debug "Checking %h'${_pcm_id}'%r" "version %h'${_pcm_version}'%r" "arch_map: %h'${_pcm_arch_map}'%r" "resolve: %h'${_pcm_resolve}'%r"

    if ! dependency "${_pcm_id}" "${_pcm_version}" "${_pcm_arch_map}" "${_pcm_resolve}"; then
      return "${?}"
    fi
  done
}

#? End Functions ----------------------------------------------------------------------------------------------------->

#? Exports ----------------------------------------------------------------------------------------------------------->
# export -f
# readonly -f

#? End Exports ------------------------------------------------------------------------------------------------------->
