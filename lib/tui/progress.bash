# vim: ft=sh

#? Environment ------------------------------------------------------------------------------------------------------->
export AVES_PROGRESS_JOBS_MAX=10
export AVES_PROGRESS_SLEEP=0.1

# Statuses
export AVES_PROGRESS_STATUS_PENDING="pending"
export AVES_PROGRESS_STATUS_RUNNING="running"
export AVES_PROGRESS_STATUS_FAILED="failed"
export AVES_PROGRESS_STATUS_SUCCESS="success"

export AVES_PROGRESS_ICON_PENDING=""
export AVES_PROGRESS_ICON_RUNNING=("⣾" "⣽" "⣻" "⢿" "⡿" "⣟" "⣯" "⣷")
export AVES_PROGRESS_ICON_FAILED=""
export AVES_PROGRESS_ICON_SUCCESS=""
export AVES_PROGRESS_ICON_SUB="󱞩"

export AVES_PROGRESS_STYLE_PENDING="${AVES_COLORS_CYAN:-}"
export AVES_PROGRESS_STYLE_RUNNING="${AVES_COLORS_MAGENTA:-}"
export AVES_PROGRESS_STYLE_FAILED="${AVES_COLORS_RED:-}"
export AVES_PROGRESS_STYLE_SUCCESS="${AVES_COLORS_GREEN:-}"
export AVES_PROGRESS_STYLE_PADDING="      "
#? End Environment --------------------------------------------------------------------------------------------------->

#? Functions --------------------------------------------------------------------------------------------------------->
aves_output_job() {

  local _title_length="${1}"
  local _title="${2}"
  local _style="${3}"
  local _icon="${4}"
  local _time="${5:-}"

  # local _hstyle _t_length _calc_length
  # _hstyle="${AVES_STYLE_CYAN:-}"
  # _dstyle="${AVES_STYLE_GREY:-}"
  # _aves_parse_string _title

  puffin get -p aves -v _hstyle "aves.colors.cyan"
  puffin get -p aves -v _dstyle "aves.colors.grey"

  if [[ -n "${_time}" ]]; then
    local m s ms

    m=$((_time / 60000))
    s=$((_time / 1000 % 60))
    ms=$((_time % 1000))

    printf >&2 -- "%b%-${_title_length}b%s%b%3s%b%s%b%02d:%02d:%03d%b%b\n" \
      "${AVES_NOFORMAT:-}" "${_title}" "${AVES_PROGRESS_STYLE_PADDING}" \
      "${_style}" "${_icon}" "${AVES_NOFORMAT:-}" \
      "${AVES_PROGRESS_STYLE_PADDING}" "${_dstyle:-}" \
      "${m}" "${s}" "${ms}" "${AVES_NOFORMAT:-}" "\033[K"
  else
    printf >&2 -- "%b%-${_title_length}b%s%b%3s%b%b\n" \
      "${AVES_NOFORMAT:-}" "${_title}" "${AVES_PROGRESS_STYLE_PADDING}" \
      "${_style}" "${_icon}" "${AVES_NOFORMAT:-}" "\033[K"
  fi
}

aves_output_sub() {
  local _line="${1}"

  # Fix carriage returns and convert to new lines
  _line="${_line//$'\r'/$'\n'}"
  # Remove empty lines and get the last line
  _line="$(sed '/^$/d' <<<"${_line}" | tail -n 1)"

  printf >&2 -- "%b%s%b%s  %s%b\n" \
    "${AVES_NOFORMAT:-}" "  " "${AVES_COLORS_GREY:-}" \
    "${AVES_PROGRESS_ICON_SUB}" "${_line}" "\033[K"
}

aves_run_jobs() {

  # PERF(VBE): So pretty.. but so slow
  local -n _procs="${1}"
  local -n _titles="${2}"

  if [[ "${#_procs[@]}" -eq 0 ]]; then
    : error "No jobs to run"
    return "${AVES_EXIT_MISSING_SCRIPT_ARGUMENTS:-1}"
  fi

  if [[ "${#_procs[@]}" -ne "${#_titles[@]}" ]]; then
    : error "The number of jobs and titles must be the same"
    return "${AVES_EXIT_MISSING_SCRIPT_ARGUMENTS:-1}"
  fi

  local -a _pids
  local -a _status
  local -a _time

  local -a _stdout

  local _title_length=0

  local _highlight _dim _t_length _title
  # puffin get -p aves -v _highlight "aves.colors.cyan"
  # puffin get -p aves -v _dim "aves.colors.grey"

  for ((i = 0; i < "${#_titles[@]}"; i++)); do

    local _title
    _title="${_titles[$i]}"
    _aves_parse_string _title
    _titles[i]="${_title}"

    if [[ "${#_title}" -gt "${_title_length}" ]]; then
      _title_length="${#_title}"
    fi
  done

  local _return_code _temp_time
  local _current_number_of_processes _number_of_processes
  _current_number_of_processes=0
  _number_of_processes="${#_procs[@]}"

  local _spinner_current _spinner_max
  _spinner_current=0
  _spinner_max="${#AVES_PROGRESS_ICON_RUNNING[@]}"

  # Initialize array
  for ((i = 0; i < "${#_procs[@]}"; i++)); do
    _status+=("${AVES_PROGRESS_STATUS_PENDING}")
  done

  local _drawn_lines _max_draw_lines
  _max_draw_lines=$((2 * _number_of_processes))
  _drawn_lines=0

  # A little late start time
  local _start_time _end_time
  aves_realtime _start_time

  local _temp_dir
  _temp_dir=$(\mktemp -d -t "${0##*/}.XXXXXXXXXX")

  cursor_blink_off
  while true; do
    _drawn_lines=0
    for ((i = 0; i < "${#_procs[@]}"; i++)); do
      if [[ "${_status[$i]}" == "${AVES_PROGRESS_STATUS_PENDING}" &&
        "${_current_number_of_processes}" -lt "${AVES_PROGRESS_JOBS_MAX}" ]]; then

        if [[ -z "${EPOCHREALTIME:-}" ]]; then
          ((_temp_time = $(date +%s%N) / 1000000))
        else
          ((_temp_time = "${EPOCHREALTIME/[^0-9]/}" / 1000))
        fi
        _time[i]="${_temp_time}"

        # Execute the job and send it to the background
        # TODO(VBE): use ${TEMPDIR:-/tmp} instead of /tmp
        _stdout[i]="${_temp_dir}/aves-progress-$$-${i}"
        _file="${_stdout[$i]}"
        \touch "${_file}"
        : info "Running job: ${_procs[$i]}"

        _cmd="${_procs[$i]%%* }"
        ${_cmd} "${_procs[$i]## *}"

        : debug "PID: $!"

        # Store the PID of the job
        _pids[i]="${!}"
        _status[i]="${AVES_PROGRESS_STATUS_RUNNING}"
        _current_number_of_processes=$((_current_number_of_processes + 1))
      fi

      if [[ "${_status[$i]}" == "${AVES_PROGRESS_STATUS_RUNNING}" ]]; then
        if ! \ps -p "${_pids[$i]}" >/dev/null; then

          wait "${_pids[$i]}"
          _return_code="${?}"

          aves_realtime _temp_time

          _time[i]=$(("${_temp_time}" - _time[i]))

          unset "_pids[$i]"

          if [[ "${_return_code}" -ne 0 ]]; then
            _status[i]="${AVES_PROGRESS_STATUS_FAILED}"
          else
            _status[i]="${AVES_PROGRESS_STATUS_SUCCESS}"
          fi

          _current_number_of_processes=$((_current_number_of_processes - 1))
          \rm -f "${_stdout[$i]}"
        fi
      fi

      case "${_status[$i]}" in
        "${AVES_PROGRESS_STATUS_PENDING}")
          aves_output_job "${_title_length}" "${_titles[$i]}" \
            "${AVES_PROGRESS_STYLE_PENDING}" "${AVES_PROGRESS_ICON_PENDING}"
          _drawn_lines=$((1 + _drawn_lines))
          ;;

        "${AVES_PROGRESS_STATUS_RUNNING}")
          aves_output_job "${_title_length}" "${_titles[$i]}" \
            "${AVES_PROGRESS_STYLE_RUNNING}" "${AVES_PROGRESS_ICON_RUNNING[${_spinner_current}]}"
          aves_output_sub "$(tail -n 1 "${_stdout[$i]}")"
          _drawn_lines=$((2 + _drawn_lines))
          ;;

        "${AVES_PROGRESS_STATUS_FAILED}")
          aves_output_job "${_title_length}" "${_titles[$i]}" \
            "${AVES_PROGRESS_STYLE_FAILED}" "${AVES_PROGRESS_ICON_FAILED}" "${_time[$i]}"
          # aves_output_sub "$(tail -n 1 "${_stdout[$i]}")"
          _drawn_lines=$((2 + _drawn_lines))
          ;;

        "${AVES_PROGRESS_STATUS_SUCCESS}")
          aves_output_job "${_title_length}" "${_titles[$i]}" \
            "${AVES_PROGRESS_STYLE_SUCCESS}" "${AVES_PROGRESS_ICON_SUCCESS}" "${_time[$i]}"
          _drawn_lines=$((1 + _drawn_lines))
          ;;
      esac
    done

    if [[ "${#_pids[@]}" -eq 0 ]]; then
      break
    fi

    _spinner_current=$(((_spinner_current + 1) % _spinner_max))

    sleep ${AVES_PROGRESS_SLEEP:-0.1}
    for ((i = 0; i < $((_max_draw_lines - _drawn_lines)); i++)); do
      printf >&2 "%b\n" "\033[K"
    done

    printf "\033[%sA" "${_max_draw_lines}"
  done

  aves_realtime _end_time

  # FIX(VBE): Is the extra line reset necessary?
  printf "%b" "\033[K"
  cursor_blink_on

  # TODO(VBE): Result Output
  # printf >&2 -- "%s %s\n" "Total time:" "$((_end_time - _start_time))ms"
}
#? End Functions ----------------------------------------------------------------------------------------------------->
