# vim: ft=sh

#? Don't Execute, Source Only ---------------------------------------------------------------------------------------->
(return 0 2>/dev/null) || {
  # PERF(VBE): Avoid the use of subshells
  printf >&2 "Script %s is meant to be sourced, not executed\n" "${BASH_SOURCE[0]##*/}"
  return 1
}

#? Aves Library ------------------------------------------------------------------------------------------------------>

#? Loaded ------------------------------------------------------------------------------------------------------------>
if [[ -n "${AVES_CORE-}" ]]; then
  : debug "Library %h'%${BASH_SOURCE[0]##*.}%n' already loaded"
  return 0
fi
#? End Loaded -------------------------------------------------------------------------------------------------------->

#? Environment Variables --------------------------------------------------------------------------------------------->
#? We are defining constants here.
#? Constants / Environment Environment Variables should be in the following format:
#? - All Uppercase, separated by underscores and declared in the top of the file.
#? - It is preferable to use either 'readonly' or 'export' to define the variable.
#?      In cases where both are needed, use 'declare -xgr'.

#? Directories ------------------------------------------------------------------------------------------------------->
case "${BASH_SOURCE:-}" in
  */*) AVES_LIB_DIR=${BASH_SOURCE%/*} ;;
  *) AVES_LIB_DIR=. ;;
esac

case "${AVES_LIB_DIR:-}" in
  */* | [!+-]* | [+-]*[!0123456789]*)
    AVES_LIB_DIR=$(cd -P -- "${PWD}" && cd -- "${AVES_LIB_DIR}" && pwd -P && echo X)
    ;;
  *) AVES_LIB_DIR=$(cd -P -- "${PWD}" && cd "./${AVES_LIB_DIR}" && pwd -P && echo X) ;;
esac || return 1

#? This is the location of the library (I hope)
declare -gxr AVES_LIB_DIR=${AVES_LIB_DIR%?X}

#? FFI Library Directory
declare -gxr AVES_FFI_DIR="${AVES_LIB_DIR}/ffi"
#? End Directories --------------------------------------------------------------------------------------------------->

#? Exit Codes -------------------------------------------------------------------------------------------------------->
#? Exit Codes Available Ranges:
#? 8-63,
#? 79-125,
#? 166-199,
#? 243-255

#? Exit Codes Reserved Ranges:
#? 0 - 7
#? 126 - 165
#? 255 [1]
#? System Exit Codes ------------------------------------------------------------------------------------------------->
declare -xgr AVES_EXIT_SUCCESS=0
declare -xgr AVES_EXIT_GENERAL_ERROR=1
declare -xgr AVES_EXIT_MISUSE_OF_SHELL_BUILTINS=2 #Hmmm 🤔

declare -xgr AVES_EXIT_UNSUPPORTED_PLATFORM=3
declare -xgr AVES_EXIT_UNSUPPORTED_VERSION=4
declare -xgr AVES_EXIT_LANG_NOT_UTF8=5
declare -xgr AVES_EXIT_MISSING_REQUIRED_PARAM=6
declare -xgr AVES_EXIT_MISSING_SCRIPT_ARGUMENTS=7

declare -xgr AVES_EXIT_COMAND_INVOKED_CANNOT_EXECUTE=126
declare -xgr AVES_EXIT_COMMAND_NOT_FOUND=127
declare -xgr AVES_EXIT_INVALID_ARGUMENT=128
declare -xgr AVES_EXIT_SCRIPT_TERMINATED_BY_CONTROL_C=130
#? End System Exit Codes --------------------------------------------------------------------------------------------->

#? AVES Core Exit Codes ----------------------------------------------------------------------------------------------->
declare -xgr AVES_EXIT_FILE_NOT_FOUND=8
declare -xgr AVES_EXIT_PARSING_ERROR=9
declare -xgr AVES_EXIT_LIBRARY_NOT_FOUND=10
#? End AVES Core Exit Codes ------------------------------------------------------------------------------------------->
#? End Exit Codes ---------------------------------------------------------------------------------------------------->

#? Helpers ----------------------------------------------------------------------------------------------------------->
# Can be used as a replacement for ' ' (space) in user facing applications.
declare -xgr AVES_SEPERATOR="⠀" # U+2800 BRAILLE PATTERN BLANK
# Define the IFS variable to be a space, tab, and newline.
declare -xgr AVES_IFS=$' \t\n'
#? End Helpers ------------------------------------------------------------------------------------------------------->

#? End Environment Variables ----------------------------------------------------------------------------------------->

#? Functions --------------------------------------------------------------------------------------------------------->
#######################################
# Initializes the library.
# This function is called when the library is sourced.
# It sets the environment variables and imports the required libraries.
# It also creates a temporary directory if one is not specified.
#
# Locale Overrides:
#  LC_MESSAGES=C - Sets the language for the messages.
#  LC_NUMERIC=C - Sets the locale for numeric formatting.
#
# Bash Options:
#   nounset - Treat unset variables as an error when substituting.
#   pipefail - The return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status.
#   errtrace - Any trap on ERR is inherited by shell functions, command substitutions, and commands executed in a subshell environment.
#
# Usage: source eob.bash [--temp-dir TEMP_DIR]
# Arguments:
#   --temp-dir - The temporary directory to use.
#               If not set, a temporary directory will be created.
# Globals:
#   AVES_TEMP_DIR - The temporary directory to use.
#   AVES_CORE - The core library flag, to prevent the library from being sourced more than once.
#
# Returns:
#  None
#######################################
__main() {
  # we unsetting the main function to prevent it from being called again
  unset -f __main

  # Treat unset variables and parameters other than the special parameters ‘@’ or ‘*’,
  # or array variables subscripted with ‘@’ or ‘*’, as an error when performing parameter expansion.
  # An error message will be written to the standard error, and a non-interactive shell will exit.
  # (-u)
  set -o nounset

  # The return value of a pipeline is the value of the last (rightmost)
  # command to exit with a non-zero status, or zero if all commands in the pipeline exit successfully.
  set -o pipefail

  # Any trap on ERR is inherited by shell functions, command substitutions,
  # and commands executed in a subshell environment.
  # The ERR trap is normally not inherited in such cases. (-E)
  set -o errtrace

  # Locale Settings
  # Overrides the locale settings, making the script independent of the system's locale settings.
  # https://unix.stackexchange.com/a/87763
  declare -x LC_MESSAGES="C" LC_NUMERIC="C" LC_ALL=""

  # Get System Information
  declare -gx AVES_PLATFORM
  declare -gx AVES_ARCHITECTURE
  declare -gx AVES_DISTRIBUTION

  aves_get_platform AVES_PLATFORM
  aves_get_arch AVES_ARCHITECTURE
  aves_get_distribution AVES_DISTRIBUTION

  readonly AVES_PLATFORM AVES_ARCHITECTURE AVES_DISTRIBUTION
  # end Get System Information

  # Load FFI (Foreign Function Interface) Library
  # This is the library that allows us to interface directly with bash
  # The source is a GO/CGO/C Library Stored in the /src/aves/ directory
  # The library is compiled and stored in the /lib/fii/ directory

  # Libraries:
  #   puffin: provides file and buffer manipulation functions (yaml, json, etc.)

  local extension
  extension="so"

  if [[ "${AVES_PLATFORM}" == "darwin" ]]; then
    extension="dylib"
  fi

  enable -f "${AVES_FFI_DIR}/aves-${AVES_PLATFORM}-${AVES_ARCHITECTURE}.${extension}" puffin || {
    printf >&2 "Error: %s\n" "Failed to load the FFI library for ${AVES_PLATFORM}-${AVES_ARCHITECTURE}"
    printf >&2 "Error: %s\n" "The library is required for the script to run"
    return "${AVES_EXIT_LIBRARY_NOT_FOUND}"
  }

  enable -f "${AVES_FFI_DIR}/aves-${AVES_PLATFORM}-${AVES_ARCHITECTURE}.${extension}" aves || {
    printf >&2 "Error: %s\n" "Failed to load the FFI library for ${AVES_PLATFORM}-${AVES_ARCHITECTURE}"
    printf >&2 "Error: %s\n" "The library is required for the script to run"
    return "${AVES_EXIT_LIBRARY_NOT_FOUND}"
  }

  printf "Loading %s\n" "aves-${AVES_PLATFORM}-${AVES_ARCHITECTURE}.${extension}"
  # Source Core libraries
  source "${AVES_LIB_DIR}/output.bash"
  source "${AVES_LIB_DIR}/semver.bash"
  source "${AVES_LIB_DIR}/dependency.bash"
  # End Source Core Libraries

  # Load Config
  puffin read -p "aves" "${AVES_LIB_DIR}/aves.yaml"

  # Check if "AVES_THEME" is set
  if ! puffin get -p "aves" -e "$.aves.theme" >/dev/null; then
    puffin read -p "aves" "${AVES_LIB_DIR}/theme.yaml"
  fi

  # Set Variables
  puffin get -p aves -v "_AVES_INT_TEMP_DIR" "$.aves.dir.temp"
  puffin get -p aves -v "AVES_DIR_WORK" "$.aves.dir.workspace"
  puffin get -p aves -v "AVES_DIR_LOCAL" "$.aves.dir.local"
  puffin get -p aves -v "AVES_DIR_BIN" "$.aves.dir.bin"

  # Set Colors
  _aves_set_colors

  export AVES_DIR_WORK _AVES_INT_TEMP_DIR AVES_DIR_LOCAL AVES_DIR_BIN

  if [[ ! -d "${AVES_DIR_BIN:-}" ]]; then
    \mkdir -p "${AVES_DIR_BIN}"
  fi

  if [[ ":${PATH}:" != *":${AVES_DIR_BIN:-}:"* ]]; then
    : debug "Adding %h'${AVES_DIR_BIN}'%r to PATH"
    export PATH="${AVES_DIR_BIN}:${PATH}"
  else
    : debug "Path: %h'${AVES_DIR_BIN}'%r already in PATH"
  fi

  # Register Dynamic Variables
  # declare -gx AVES_TEMP_DIR
  aves dynamic register -n AVES_TEMP_DIR -f _aves_temp_dir

  # TODO(VBE): Add functions for adding traps
  # trap 'stacktrace ; [[ -d "${_AVES_INT_TEMP_DIR}" ]] && { : debug "Cleaning temp_dir: %h'"'"'${_AVES_INT_TEMP_DIR:-}'"'"'%r" ; rm -rf "${_AVES_INT_TEMP_DIR:-}" ; }' EXIT
  # trap '_aves_ec=${?}; stacktrace; aves_err_trap $@; unset _aves_ec' ERR

  # Prevent the library from being sourced more than once
  declare -gxr AVES_CORE=1
}

#######################################
# Gets the OS platform name.
#
# Usage: aves_get_platform [return_variable]
# Example:
#   local platform
#   get_platform platform
#   printf "%s\n" "${platform}"
#
# Arguments:
#   nameref return_variable - The variable to store the platform name.
#
# Returns:
#   0 - If the platform name was successfully retrieved.
#   1 - If the platform name could not be retrieved.
#
#######################################
aves_get_platform() {
  if [[ -z "${1:-}" ]]; then
    : error "Missing required parameter for aves_get_platform()"
    return "${AVES_EXIT_MISSING_REQUIRED_PARAM}"
  fi

  local -n _l_aves_platform
  _l_aves_platform="${1:-}"

  # this should be automatically set by the shell
  _l_aves_platform="${OSTYPE//[0-9.]/}"

  if [[ -z "${_l_aves_platform:-}" ]]; then
    _l_aves_platform="$(uname -s)"
  fi

  case "${_l_aves_platform}" in
    [Ll]inux*) _l_aves_platform=linux ;;
    [Dd]arwin*) _l_aves_platform=darwin ;;
    CYGWIN*) _l_aves_platform=cygwin ;;
    MINGW*) _l_aves_platform=mingw ;;
    *)
      _l_aves_platform=unknown
      return "${AVES_EXIT_UNSUPPORTED_PLATFORM}"
      ;;
  esac

  return "${AVES_EXIT_SUCCESS}"
}

#######################################
# Gets the OS architecture name.
# This function is useful for determining the architecture of the system.
#
# Usage: aves_get_arch [return_variable]
# Example:
#  local arch
#  aves_get_arch arch
#  printf "%s\n" "${arch}"
#
# Arguments:
#  nameref return_variable - The variable to store the architecture name.
#                            The architecture name will be stored in this variable.
#
# Returns:
# 0 - If the architecture name was successfully retrieved.
# 1 - If the architecture name could not be retrieved.
#
#######################################
aves_get_arch() {

  if [[ -z "${1:-}" ]]; then
    : error "Missing required parameter for aves_get_architecture()"
    return "${AVES_EXIT_MISSING_REQUIRED_PARAM}"
  fi

  local -n _l_aves_arch
  _l_aves_arch="${1}"

  _l_aves_arch="$(uname -m)"

  case "${_l_aves_arch}" in
    x86_64) _l_aves_arch=amd64 ;; # or AMD64 or Intel64 or x64 or x86-64
    i*86) _l_aves_arch=386 ;;
    armv7l) _l_aves_arch=arm ;;
    aarch64) _l_aves_arch=arm64 ;;
    arm64) _l_aves_arch=arm64 ;;
    *) # This is a catch-all for other architectures
      _l_aves_arch=unknown
      return "${AVES_EXIT_UNSUPPORTED_PLATFORM}"
      ;;
  esac

  return "${AVES_EXIT_SUCCESS}"
}

#######################################
# Gets the OS distribution name.
#
# Usage: aves_get_distribution [return_variable]
# Example:
#   local dist
#   aves_get_distribution dist
#   printf "%s\n" "${dist}"
#
# Arguments:
#  nameref return_variable - The variable to store the distribution name.
#                            The distribution name will be stored in this variable.
#
# Returns:
# 0 - If the distribution name was successfully retrieved.
# 1 - If the distribution name could not be retrieved.
#
#######################################
aves_get_distribution() {
  # PERF(VBE): Avoid the use of subshells
  if [[ -z "${1:-}" ]]; then
    : error "Missing required parameter for aves_get_distribution()"
    return "${AVES_EXIT_MISSING_REQUIRED_PARAM}"
  fi

  local -n _l_aves_dist
  _l_aves_dist="${1}"

  if [[ -f /etc/os-release ]]; then
    # shellcheck disable=SC1091
    _l_aves_dist="$(source /etc/os-release && printf "%s" "${ID}")"
  elif command -v lsb_release &>/dev/null; then
    _l_aves_dist="$(lsb_release -si)"
  elif [[ -f /etc/lsb-release ]]; then
    # shellcheck disable=SC1091
    _l_aves_dist="$(source /etc/lsb-release && printf "%s" "${DISTRIB_ID}")"
  elif [[ -f /etc/debian_version ]]; then
    _l_aves_dist=debian
  elif [[ -f /etc/fedora-release ]]; then
    _l_aves_dist=fedora
  elif [[ -f /etc/redhat-release ]]; then
    _l_aves_dist=redhat
  else
    _l_aves_dist=unknown
    return "${AVES_EXIT_UNSUPPORTED_PLATFORM}"
  fi

  return "${AVES_EXIT_SUCCESS}"
}

#######################################
# Imports a library from the library directory.
# If the library is a directory, it will import the main library file.
# If the library is a file, it will import the library file, and the parent library file if it exists.
# A main library file is a file with the same name as the directory.
#
# Usage: import library_slug
# Example: import utils/arrays
# Arguments:
#   library_slug - The name of the library to import, without the file extension.
#              If the library is in a subdirectory, use the directory name as a prefix.
#              Example: import utils/arrays
#              This will import the 'arrays' library from the 'utils' directory.
#              As well as the 'utils' main library file if it exists.
#
# Returns:
#   0 - If the library was imported successfully.
#   10 - If the library was not found.
#
# Stdout:
#   None
#######################################
import() {
  local library parent
  library="${1:?Missing required parameter: library}"

  if [[ -d "${AVES_LIB_DIR}/${library}" ]]; then
    : debug "Importing library: %h'${library}'%r"

    if [[ -f "${AVES_LIB_DIR}/${library}/${library}.bash" ]]; then

      source "${AVES_LIB_DIR}/${library}/${library}.bash"

      : verbose "Library: %h'${library}'%r imported successfully"
      return "${AVES_EXIT_SUCCESS}"
    else
      : error "Library: %h'${library}'%r is a directory, but no main file found" \
        "Please make sure the library has a %h'${library}.bash'%r file"
      return "${AVES_EXIT_LIBRARY_NOT_FOUND}"
    fi
  elif [[ -f "${AVES_LIB_DIR}/${library}.bash" ]]; then
    : debug "Importing library: %h'${library}.bash'%r"

    parent="${library%/*}"
    if [[ -f "${AVES_LIB_DIR}/${parent}/${parent}.bash" ]]; then
      : debug "Importing parent library: %h'${parent}'%r"

      source "${AVES_LIB_DIR}/${parent}/${parent}.bash"
    fi

    source "${AVES_LIB_DIR}/${library}.bash"
    : verbose "Library: %h'${library}.bash'%r imported successfully"
    return "${AVES_EXIT_SUCCESS}"
  fi

  : error "Library: %h'${library}'%r not found" \
    "Please make sure the library exists in the %h'${AVES_LIB_DIR}'%r directory"

  return "${AVES_EXIT_LIBRARY_NOT_FOUND}"
}

#######################################
# Outputs the name of an exit code, given the exit code number.
# If the exit code is not found, the function will return 1 (General Error).
# The exit code name is returned in the 'return_var' variable.
#
# Usage: eob::exit_code_name return_var exit_code
# Example:
#   declare exit_name
#   aves_exit_code_name exit_name 0
#   printf "Exit code name: %s\n" "${exit_name}" # Exit code name: AVES_EXIT_SUCCESS
#
# Arguments:
#   return_var - The variable to store the exit code name.
#   exit_code - The exit code number to get the name of.
#
# Returns:
#   0 - If the exit code name was found.
#   1 - If the exit code name was not found.
#
# Stdout:
#   None
#######################################
aves_exit_code_name() {
  local return_var exit_code exit_name

  return_var="${1:?Missing required parameter: return_var}"
  exit_code="${2:?Missing required parameter: exit_code}"

  for exit_name in "${!AVES_EXIT_@}"; do
    if [[ "${!exit_name}" == "${exit_code}" ]]; then
      printf -v "${return_var}" "%s" "${exit_name}"
      return "${AVES_EXIT_SUCCESS}"
    fi
  done

  return "${AVES_EXIT_GENERAL_ERROR}"
}

#######################################
# This is a dynamic function that creates a temporary directory.
# The temporary directory is created upon invocation of "${AVES_TEMP_DIR}".
#######################################
_aves_temp_dir() {
  if [[ -z "${_AVES_INT_TEMP_DIR:-}" ]]; then
    _AVES_INT_TEMP_DIR=$(\mktemp -d -t "${0##*/}.XXXXXXXXXX")
    declare -gxr _AVES_INT_TEMP_DIR="${_AVES_INT_TEMP_DIR}"
    : debug "Temporary directory: %h'${_AVES_INT_TEMP_DIR}'%r created"

    aves dynamic set -n AVES_TEMP_DIR -v "${_AVES_INT_TEMP_DIR}"
    declare -gxr AVES_TEMP_DIR="${_AVES_INT_TEMP_DIR}"
  fi
}

aves_realtime() {
  local _return_value

  if [[ -z "${EPOCHREALTIME:-}" ]]; then
    ((_return_value = $(date +%s%N) / 1000000))
  else
    ((_return_value = "${EPOCHREALTIME/[^0-9]/}" / 1000))
  fi

  if [[ -n "${1:-}" ]]; then
    printf -v "${1}" "%s" "${_return_value}"
  else
    printf "%s" "${_return_value}"
  fi
}

#? End Functions ----------------------------------------------------------------------------------------------------->

#? Exports ----------------------------------------------------------------------------------------------------------->
readonly -f aves_get_platform aves_get_arch aves_get_distribution \
  import aves_exit_code_name aves_realtime
export -f aves_get_platform aves_get_arch aves_get_distribution \
  import aves_exit_code_name aves_realtime
#? End Exports ------------------------------------------------------------------------------------------------------->

#? Main -------------------------------------------------------------------------------------------------------------->
__main "${@}"
#? End Main ---------------------------------------------------------------------------------------------------------->
