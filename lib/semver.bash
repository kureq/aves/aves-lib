# vim: ft=sh
#? Check if Library is already loaded -------------------------------------------------------------------------------->
if [[ -n "${AVES_LIBRARY_CORE-}" ]]; then
  : debug "Library: %h'${BASH_SOURCE[0]##*/}'%r is already loaded, skipping"
  return 0
fi
#? End Check --------------------------------------------------------------------------------------------------------->

#? Environment Variables --------------------------------------------------------------------------------------------->
declare -grx AVES_REGEX_ERE_SEMVER="([0-9]+)\.([0-9]+)\.([0-9]+)"
declare -grx AVES_REGEX_ERE_SEMVER_PART="([0-9]+)"
#? End Environment Variables ----------------------------------------------------------------------------------------->

#? Functions --------------------------------------------------------------------------------------------------------->

#######################################
# Check if a version is a semantic version string
#
# Arguments:
#   version - The version to check
#
# Returns:
#   0 if the version is a semantic version string, 1 otherwise
#
#######################################
aves_is_semver() {
  local version="${1}"
  [[ "${version}" =~ ^[0-9]+\.[0-9]+\.[0-9]+(-[0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*)?(\+[0-9A-Za-z-]+)?$ ]]
}

#######################################
# Check if a version is a semantic version part
#
# Arguments:
#  version - The version to check
#
# Returns:
#  0 if the version is a semantic version part, 1 otherwise
#
#######################################
aves_is_semver_part() {
  local version="${1}"
  [[ "${version}" =~ ^[0-9]+$ ]]
}

#######################################
# Split a version string into version and operator
# If the version string contains an operator, it will be split into the version and operator
#
# Usage: aves_split_version_operator <version_string> <return_version> <return_operator>
# Example:
#   declare version operator
#   aves_split_version_operator ">=1.0.0" version operator
#   printf "Version: %s, Operator: %s\n" "${version}" "${operator}"
#
# Arguments:
#   version_string - The version string to split
#   return_version - The variable to store the version
#   return_operator - The variable to store the operator
#
# Returns:
#   0 if the version string was split successfully, 1 otherwise
#
#######################################
aves_split_version_operator() {
  local _version _operator

  if [[ "${1:-}" =~ ([=<>!]+) ]]; then
    _version="${1##*[=<>!]}"
    _operator="${1/"${_version}"/}"

    printf -v "${2:?Missing return_version}" "%s" "${_version}"
    printf -v "${3:?Missing return_operator}" "%s" "${_operator}"

    return 0
  fi

  printf -v "${2:?Missing return_version}" "%s" "${1}"
  return 0
}

#######################################
# Split a version string into major, minor, and patch parts
#
# Usage: aves_semvar_split <return_major> <return_minor> <return_patch> <version_string>
# Example:
#   declare major minor patch
#   aves_semvar_split major minor patch "10.20.5"
#   printf "Major: %s, Minor: %s, Patch: %s\n" "${major}" "${minor}" "${patch}"
#
# Arguments:
#   return_major - The variable to store the major version
#   return_minor - The variable to store the minor version
#   return_patch - The variable to store the patch version
#   version_string - The version string to split
#
# Returns:
#  0 if the version string was split successfully, 1 otherwise
#  The major, minor, and patch variables will be set in the calling environment
#######################################
aves_semvar_split() {
  if [[ "${#}" -ne 4 ]]; then
    : error "Invalid number of arguments" \
      "Expected: %h4" \
      "Received: %h${#}" \
      "Usage: %haves_version_to_semantic <return_major> <return_minor> <return_patch> <version_string>"
    return "${AVES_EXIT_INVALID_ARGUMENTS-1}"
  fi

  local return_major return_minor return_patch version_string

  return_major="${1:?Missing return_major}"
  return_minor="${2:?Missing return_minor}"
  return_patch="${3:?Missing return_patch}"
  version_string="${4:?Missing version_string}"

  # We are using the ERE to match the version string, and then we are using the BASH_REMATCH array to get the values
  # A semver string is in the format of major.minor.patch
  if [[ "${version_string}" =~ ${AVES_REGEX_ERE_SEMVER} ]]; then
    major="${BASH_REMATCH[1]}"
    minor="${BASH_REMATCH[2]}"
    patch="${BASH_REMATCH[3]}"
  elif [[ "${version_string}" =~ ${AVES_REGEX_ERE_SEMVER_PART} ]]; then
    # We are iterating over the version string, and we are removing the major, minor, and patch versions as best as we can
    : warning "Version string is not in the correct format for semantic versioning" \
      "%h'${version_string}'"

    major="${BASH_REMATCH[1]}"
    # Remove the major version from the string
    version_string="${version_string#*"${major}"}"

    # We are unsetting the BASH_REMATCH array to prevent any issues with the next match
    unset BASH_REMATCH
    [[ "${version_string}" =~ ${AVES_REGEX_ERE_SEMVER_PART} ]] || {
      : warning "Version string is missing the minor version" \
        "%h'${version_string}'"
    }

    minor="${BASH_REMATCH[1]-}"
    version_string="${version_string#*"${minor}"}"

    unset BASH_REMATCH
    [[ "${version_string}" =~ ${AVES_REGEX_ERE_SEMVER_PART} ]] || {
      : warning "Version string is missing the patch version" \
        "%h'${version_string}'"
    }

    patch="${BASH_REMATCH[1]-}"
    version_string="${version_string#*"${patch}"}"
  else
    : error "Unable to convert string %h'${version_string}'%r to semantic version" \
      "%h'${version_string}'"
    return "${AVES_EXIT_INVALID_ARGUMENTS-1}"
  fi

  printf -v "${return_major}" "%s" "${major}"
  printf -v "${return_minor}" "%s" "${minor}"
  printf -v "${return_patch}" "%s" "${patch}"
}

#######################################
# Compare two semantic version strings using the specified operator
#
# Usage: aves_compare_semver <version1> <version2> [operator (default: >=)]
# Example:
#   if aves_compare_semver "1.0.1" "1.0.0" ">" ; then
#     printf "Version 1 is greater than Version 2"
#   fi
#
# Arguments:
#   version1 - The first version to compare
#   version2 - The second version to compare
#   operator - The operator to use for comparison, default is >= (>, <, >=, <=, !=, ==)
#           > - Greater than
#           < - Less than
#           >= - Greater than or equal to
#           <= - Less than or equal to
#           != - Not equal to
#           == - Equal to
#
# Returns:
#   0 - if the comparison is true, 1 otherwise
#
#######################################
aves_compare_semver() {

  if [[ "${#}" -lt 2 ]]; then
    : error "Invalid number of arguments" \
      "Expected: %h2" \
      "Received: %h${#}" \
      "Usage: %haves_version_compare_semantic <version1> <version2> [operator]"
    return "${AVES_EXIT_INVALID_ARGUMENTS-1}"
  fi

  local _aves_semvar_version1 _aves_semvar_version2 _aves_semvar_operator

  if [[ "${1:?Missing version}" =~ ([=<>!]+) ]]; then
    aves_split_version_operator "${1}" _aves_semvar_version1 _aves_semvar_operator
  else
    _aves_semvar_version1="${1:?Missing version1}"
  fi

  if [[ "${2:?Missing version}" =~ ([=<>!]+) ]]; then
    aves_split_version_operator "${2}" _aves_semvar_version2 _aves_semvar_operator
  else
    _aves_semvar_version2="${2:?Missing version2}"
  fi

  _aves_semvar_operator="${_aves_semvar_operator:-${3:->=}}"

  if ! aves_is_semver "${_aves_semvar_version1}"; then
    : error "Version 1 is not a semantic version string" \
      "%h'${_aves_semvar_version1}'"
    return "${AVES_EXIT_INVALID_ARGUMENTS-1}"
  fi

  if ! aves_is_semver "${_aves_semvar_version2}"; then
    : error "Version 2 is not a semantic version string" \
      "%h'${_aves_semvar_version2}'"
    return "${AVES_EXIT_INVALID_ARGUMENTS-1}"
  fi

  local major1 minor1 patch1
  local major2 minor2 patch2

  aves_semvar_split major1 minor1 patch1 "${_aves_semvar_version1}"
  aves_semvar_split major2 minor2 patch2 "${_aves_semvar_version2}"

  : debug "Comparing %h'${_aves_semvar_version1}'%r %h'${_aves_semvar_operator}'%r %h'${_aves_semvar_version2}'"

  case "${_aves_semvar_operator}" in
    ">")
      if [[ "${major1}" -gt "${major2}" ]]; then
        return 0
      elif [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -gt "${minor2}" ]]; then
          return 0
        elif [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -gt "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;
    "<")
      if [[ "${major1}" -lt "${major2}" ]]; then
        return 0
      elif [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -lt "${minor2}" ]]; then
          return 0
        elif [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -lt "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;
    ">=")
      if [[ "${major1}" -gt "${major2}" ]]; then
        return 0
      elif [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -gt "${minor2}" ]]; then
          return 0
        elif [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -ge "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;
    "<=")
      if [[ "${major1}" -le "${major2}" ]]; then
        return 0
      elif [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -le "${minor2}" ]]; then
          return 0
        elif [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -le "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;

    "!=")
      if [[ "${major1}" -ne "${major2}" ]]; then
        return 0
      elif [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -ne "${minor2}" ]]; then
          return 0
        elif [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -ne "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;

    "==" | "=")
      if [[ "${major1}" -eq "${major2}" ]]; then
        if [[ "${minor1}" -eq "${minor2}" ]]; then
          if [[ "${patch1}" -eq "${patch2}" ]]; then
            return 0
          fi
        fi
      fi
      ;;
    *) : error "Invalid operator: ${_aves_semvar_operator}" && return "${AVES_EXIT_INVALID_OPERATOR-1}" ;;
  esac

  return "${AVES_EXIT_PARSING_ERROR-1}"
}

#? End Functions ----------------------------------------------------------------------------------------------------->

export -f aves_compare_semver aves_semvar_split aves_is_semver_part aves_is_semver aves_split_version_operator
readonly -f aves_compare_semver aves_semvar_split aves_is_semver_part aves_is_semver aves_split_version_operator
